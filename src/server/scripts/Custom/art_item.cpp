#include "Define.h"
#include "GossipDef.h"
#include "Item.h"
#include "Player.h"
#include "ScriptedGossip.h"
#include "ScriptMgr.h"
#include "Spell.h"
#include "ItemEnchantmentMgr.h"

#define MENU_ID 123 // Our menuID used to match the sent menu to select hook (playerscript)
uint32 enchantmentIds[] = { 4000, 4001, 4002 };
enum
{
    WORK_GOSSIP_MESSAGE = 60001,
    ENCHAT_PRIEST_LVL1_1 = 4000,   //
    ENCHAT_PRIEST_LVL1_2 = 4001,   // Ид рисованных инчантов (жрец)
    ENCHAT_PRIEST_LVL1_3 = 4002,   //
    ITEM_ART = 38644,
};


/*class artt_command : public CommandScript
{
public:
	artt_command() : CommandScript("artt_command") {}

	std::vector<ChatCommand> GetCommands() const override
	{
		static std::vector<ChatCommand> commandTable = // .commands
		{
			{ "art",				SEC_PLAYER,			false, &HandleartCommand,	"" }
		};

		return commandTable;
	}

	static bool HandleartCommand(ChatHandler *handler, const char *args)
	{
		Player* player = handler->GetSession()->GetPlayer();
		ClearGossipMenuFor(player);  
		AddGossipItemFor(player, 0, "Тест1", GOSSIP_SENDER_MAIN, 1);
		AddGossipItemFor(player, 0, "Тест2", GOSSIP_SENDER_MAIN, 2);

		player->PlayerTalkClass->GetGossipMenu().SetMenuId(MENU_ID);

		SendGossipMenuFor(player, DEFAULT_GOSSIP_MESSAGE, player->GetGUID());
	}
};
*/

class artt_command : public ItemScript
{
public:
    artt_command() : ItemScript("artt_command") { }

    bool OnUse(Player* player, Item* item, SpellCastTargets const& /*targets*/) override // Any hook here
    {
        QueryResult home = CharacterDatabase.PQuery("SELECT * FROM player_art WHERE guid = '%u'", player->GetGUID());
        if (!home)
        {
            CharacterDatabase.PExecute("INSERT INTO  `player_art` (`guid`, `level_art`) VALUES('%u', '0')", player->GetGUID());
        } 
        player->PlayerTalkClass->ClearMenus();
        player->ADD_GOSSIP_ITEM(NULL, "|TInterface/ICONS/Inv_misc_note_05:45:45|t Информация о артефакте на сервере", GOSSIP_SENDER_MAIN, 1);
        player->ADD_GOSSIP_ITEM(NULL, "|TInterface/ICONS/Inv_jewelry_ring_10:45:45|t Управление Артефактом", GOSSIP_SENDER_MAIN, 2);
        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
        return false; // Cast the spell on use normally
    }

    void OnGossipSelect(Player* player, Item* item, uint32 sender, uint32 action) override
    {
        player->PlayerTalkClass->ClearMenus();
        QueryResult level_0 = CharacterDatabase.PQuery("SELECT * FROM player_art WHERE guid = '%u' AND level_art = '0'", player->GetGUID()); //первый уровень инчантов
        QueryResult level_1 = CharacterDatabase.PQuery("SELECT * FROM player_art WHERE guid = '%u' AND level_art = '1'", player->GetGUID()); //второй уровень инчантов
        QueryResult level_2 = CharacterDatabase.PQuery("SELECT * FROM player_art WHERE guid = '%u' AND level_art = '2'", player->GetGUID());  // третий уровень инчантов
        QueryResult level_3 = CharacterDatabase.PQuery("SELECT * FROM player_art WHERE guid = '%u' AND level_art = '3'", player->GetGUID());

        // Item* _itr = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FINGER1); //Проверка на наличие вещи на игроке

        uint32 itemart_count = player->GetItemCount(ITEM_ART); //Количество силы артефакта

        switch (action)
        {
            case 2:
            {
                if (IsHaveItem(player))
                    AddGossipEnchantment(player);
                else
                {
                    ChatHandler(player->GetSession()).SendSysMessage("|cffB22222Артефакт не найден! Убедитесь, что артефакт находится в слоте первого кольца.|r");
                    player->PlayerTalkClass->SendCloseGossip();
                }

                player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                break;
            }
            case 100: //проверка на класс и левел артефакта(Обновить бонус)
                player->PlayerTalkClass->ClearMenus();
                switch (player->getClass())
                {
                    case (CLASS_PRIEST):
                        if (level_0)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "[Артефакт 1 уровня]  || Бонус: +3000 Скорости/+ 2650000 Силы Заклинаний/+ 2450000 Выносливости", GOSSIP_SENDER_MAIN, 3);
                            player->ADD_GOSSIP_ITEM(NULL, "[Артефакт 1 уровня]  || Бонус: +3000 Скорости/+ 2650000 Интелекта/+ 2900000 Выносливости", GOSSIP_SENDER_MAIN, 4);
                            player->ADD_GOSSIP_ITEM(NULL, "[Артефакт 1 уровня]  || Бонус: +3000 Скорости/+ 2650000 Духа/+ 900000 Выносливости", GOSSIP_SENDER_MAIN, 5);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_1)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 2 уровня инчантов", GOSSIP_SENDER_MAIN, 6);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_2)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 3 уровня инчантов", GOSSIP_SENDER_MAIN, 7);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_3)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 4 уровня инчантов", GOSSIP_SENDER_MAIN, 8);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                       break;
                    case (CLASS_DEATH_KNIGHT):
                        if (level_0)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 1 уровня инчантов", GOSSIP_SENDER_MAIN, 9);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_1)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 2 уровня инчантов", GOSSIP_SENDER_MAIN, 10);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_2)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 3 уровня инчантов", GOSSIP_SENDER_MAIN, 11);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_3)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 4 уровня инчантов", GOSSIP_SENDER_MAIN, 12);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        break;
                    case (CLASS_DRUID):
                        if (level_0)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 1 уровня инчантов", GOSSIP_SENDER_MAIN, 13);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_1)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 2 уровня инчантов", GOSSIP_SENDER_MAIN, 14);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_2)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 3 уровня инчантов", GOSSIP_SENDER_MAIN, 15);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_3)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 4 уровня инчантов", GOSSIP_SENDER_MAIN, 16);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        break;
                    case (CLASS_HUNTER):
                        if (level_0)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 1 уровня инчантов", GOSSIP_SENDER_MAIN, 17);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_1)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 2 уровня инчантов", GOSSIP_SENDER_MAIN, 18);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_2)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 3 уровня инчантов", GOSSIP_SENDER_MAIN, 19);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_3)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 4 уровня инчантов", GOSSIP_SENDER_MAIN, 20);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        break;
                    case (CLASS_MAGE):
                        if (level_0)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 1 уровня инчантов", GOSSIP_SENDER_MAIN, 21);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_1)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 2 уровня инчантов", GOSSIP_SENDER_MAIN, 22);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_2)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 3 уровня инчантов", GOSSIP_SENDER_MAIN, 23);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_3)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 4 уровня инчантов", GOSSIP_SENDER_MAIN, 24);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        break;
                    case (CLASS_PALADIN):
                        if (level_0)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 1 уровня инчантов", GOSSIP_SENDER_MAIN, 25);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_1)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 2 уровня инчантов", GOSSIP_SENDER_MAIN, 26);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_2)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 3 уровня инчантов", GOSSIP_SENDER_MAIN, 27);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_3)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 4 уровня инчантов", GOSSIP_SENDER_MAIN, 28);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        break;
                    case (CLASS_ROGUE):
                        if (level_0)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 1 уровня инчантов", GOSSIP_SENDER_MAIN, 29);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_1)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 2 уровня инчантов", GOSSIP_SENDER_MAIN, 30);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_2)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 3 уровня инчантов", GOSSIP_SENDER_MAIN, 31);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_3)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 4 уровня инчантов", GOSSIP_SENDER_MAIN, 32);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        break;
                    case (CLASS_WARLOCK):
                        if (level_0)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 1 уровня инчантов", GOSSIP_SENDER_MAIN, 33);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_1)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 2 уровня инчантов", GOSSIP_SENDER_MAIN, 34);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_2)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 3 уровня инчантов", GOSSIP_SENDER_MAIN, 35);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_3)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 4 уровня инчантов", GOSSIP_SENDER_MAIN, 36);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        break;
                    case (CLASS_WARRIOR):
                        if (level_0)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 1 уровня инчантов", GOSSIP_SENDER_MAIN, 37);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_1)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 2 уровня инчантов", GOSSIP_SENDER_MAIN, 38);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_2)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 3 уровня инчантов", GOSSIP_SENDER_MAIN, 39);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_3)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 4 уровня инчантов", GOSSIP_SENDER_MAIN, 40);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        break;
                    case (CLASS_SHAMAN):
                        if (level_0)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 1 уровня инчантов", GOSSIP_SENDER_MAIN, 41);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_1)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 2 уровня инчантов", GOSSIP_SENDER_MAIN, 42);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_2)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 3 уровня инчантов", GOSSIP_SENDER_MAIN, 43);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        if (level_3)
                        {
                            player->ADD_GOSSIP_ITEM(NULL, "Разработка 4 уровня инчантов", GOSSIP_SENDER_MAIN, 44);
                            player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                        }
                        break;
                       
                }
                break;
            case 3:
                if (itemart_count == 150)
                {
                    item->ClearEnchantment(PERM_ENCHANTMENT_SLOT);
                    //player->ApplyEnchantment(item, PERM_ENCHANTMENT_SLOT, true);
                    item->SetEnchantment(PERM_ENCHANTMENT_SLOT, ENCHAT_PRIEST_LVL1_1, 0, 0);
                    ChatHandler(player->GetSession()).SendSysMessage("|cff00FA9AБонус успешно обновлен у вашего артефакта, сделайте релог|r");
                    player->DestroyItemCount(itemart_count, 150, true);  
                    player->CLOSE_GOSSIP_MENU();
                }
                else
                {
                    ChatHandler(player->GetSession()).SendSysMessage("|cffB22222Бонус не обновлен.|r|cffFFAEB9Проверьте наличие 'Cилы артефакта'/150|r");
                    player->CLOSE_GOSSIP_MENU();
                }
                break;
            case 4:
                if (itemart_count == 150)
                {
                    item->ClearEnchantment(PERM_ENCHANTMENT_SLOT);
                    //player->ApplyEnchantment(item, PERM_ENCHANTMENT_SLOT, true);
                    item->SetEnchantment(PERM_ENCHANTMENT_SLOT, ENCHAT_PRIEST_LVL1_2, 0, 0);
                    ChatHandler(player->GetSession()).SendSysMessage("|cff00FA9AБонус успешно обновлен у вашего артефакта, сделайте релог|r");
                    player->CLOSE_GOSSIP_MENU();
                }
                else
                {
                    ChatHandler(player->GetSession()).SendSysMessage("|cffB22222Бонус не обновлен.|r|cffFFAEB9Проверьте наличие 'Cилы артефакта'/150|r");
                    player->CLOSE_GOSSIP_MENU();
                }
                break;
            case 5:
                if (itemart_count == 150)
                {
                    item->ClearEnchantment(PERM_ENCHANTMENT_SLOT);
                    //player->ApplyEnchantment(item, PERM_ENCHANTMENT_SLOT, true);
                    item->SetEnchantment(PERM_ENCHANTMENT_SLOT, ENCHAT_PRIEST_LVL1_3, 0, 0);
                    ChatHandler(player->GetSession()).SendSysMessage("|cff00FA9AБонус успешно обновлен у вашего артефакта, сделайте релог|r");
                    player->CLOSE_GOSSIP_MENU();
                }
                else
                {
                    ChatHandler(player->GetSession()).SendSysMessage("|cffB22222Бонус не обновлен.|r|cffFFAEB9Проверьте наличие 'Cилы артефакта'/150|r");
                    player->CLOSE_GOSSIP_MENU();
                }
                break;
            case 101:
                player->PlayerTalkClass->SendCloseGossip();
                break;
            case 102: //проверка на класс и левел артефакта ( добавить бонус)
                player->PlayerTalkClass->ClearMenus();
                switch (player->getClass())
                {
                case (CLASS_PRIEST):
                    if (level_0)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "[Артефакт 1 уровня]  || Бонус: +3000 Скорости/+ 2650000 Силы Заклинаний/+ 2450000 Выносливости", GOSSIP_SENDER_MAIN, 1001);
                        player->ADD_GOSSIP_ITEM(NULL, "[Артефакт 1 уровня]  || Бонус: +3000 Скорости/+ 2650000 Интелекта/+ 2900000 Выносливости", GOSSIP_SENDER_MAIN, 1002);
                        player->ADD_GOSSIP_ITEM(NULL, "[Артефакт 1 уровня]  || Бонус: +3000 Скорости/+ 2650000 Духа/+ 900000 Выносливости", GOSSIP_SENDER_MAIN, 1003);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_1)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 2 уровня инчантов", GOSSIP_SENDER_MAIN, 1004);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_2)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 3 уровня инчантов", GOSSIP_SENDER_MAIN, 1005);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_3)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 4 уровня инчантов", GOSSIP_SENDER_MAIN, 1006);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    break;
                case (CLASS_DEATH_KNIGHT):
                    if (level_0)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 1 уровня инчантов", GOSSIP_SENDER_MAIN, 1007);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_1)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 2 уровня инчантов", GOSSIP_SENDER_MAIN, 1008);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_2)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 3 уровня инчантов", GOSSIP_SENDER_MAIN, 1009);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_3)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 4 уровня инчантов", GOSSIP_SENDER_MAIN, 1010);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    break;
                case (CLASS_DRUID):
                    if (level_0)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 1 уровня инчантов", GOSSIP_SENDER_MAIN, 1011);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_1)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 2 уровня инчантов", GOSSIP_SENDER_MAIN, 1012);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_2)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 3 уровня инчантов", GOSSIP_SENDER_MAIN, 1013);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_3)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 4 уровня инчантов", GOSSIP_SENDER_MAIN, 1014);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    break;
                case (CLASS_HUNTER):
                    if (level_0)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 1 уровня инчантов", GOSSIP_SENDER_MAIN, 1015);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_1)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 2 уровня инчантов", GOSSIP_SENDER_MAIN, 1016);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_2)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 3 уровня инчантов", GOSSIP_SENDER_MAIN, 1017);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_3)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 4 уровня инчантов", GOSSIP_SENDER_MAIN, 1018);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    break;
                case (CLASS_MAGE):
                    if (level_0)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 1 уровня инчантов", GOSSIP_SENDER_MAIN, 1019);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_1)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 2 уровня инчантов", GOSSIP_SENDER_MAIN, 1020);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_2)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 3 уровня инчантов", GOSSIP_SENDER_MAIN, 1021);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_3)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 4 уровня инчантов", GOSSIP_SENDER_MAIN, 1022);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    break;
                case (CLASS_PALADIN):
                    if (level_0)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 1 уровня инчантов", GOSSIP_SENDER_MAIN, 1023);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_1)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 2 уровня инчантов", GOSSIP_SENDER_MAIN, 1024);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_2)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 3 уровня инчантов", GOSSIP_SENDER_MAIN, 1025);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_3)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 4 уровня инчантов", GOSSIP_SENDER_MAIN, 1026);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    break;
                case (CLASS_ROGUE):
                    if (level_0)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 1 уровня инчантов", GOSSIP_SENDER_MAIN, 1027);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_1)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 2 уровня инчантов", GOSSIP_SENDER_MAIN, 1028);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_2)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 3 уровня инчантов", GOSSIP_SENDER_MAIN, 1029);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_3)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 4 уровня инчантов", GOSSIP_SENDER_MAIN, 1030);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    break;
                case (CLASS_WARLOCK):
                    if (level_0)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 1 уровня инчантов", GOSSIP_SENDER_MAIN, 1034);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_1)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 2 уровня инчантов", GOSSIP_SENDER_MAIN, 1035);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_2)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 3 уровня инчантов", GOSSIP_SENDER_MAIN, 1036);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_3)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 4 уровня инчантов", GOSSIP_SENDER_MAIN, 1037);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    break;
                case (CLASS_WARRIOR):
                    if (level_0)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 1 уровня инчантов", GOSSIP_SENDER_MAIN, 1038);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_1)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 2 уровня инчантов", GOSSIP_SENDER_MAIN, 1039);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_2)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 3 уровня инчантов", GOSSIP_SENDER_MAIN, 1040);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_3)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 4 уровня инчантов", GOSSIP_SENDER_MAIN, 1041);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    break;
                case (CLASS_SHAMAN):
                    if (level_0)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 1 уровня инчантов", GOSSIP_SENDER_MAIN, 1042);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_1)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 2 уровня инчантов", GOSSIP_SENDER_MAIN, 1043);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_2)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 3 уровня инчантов", GOSSIP_SENDER_MAIN, 1044);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    if (level_3)
                    {
                        player->ADD_GOSSIP_ITEM(NULL, "Разработка 4 уровня инчантов", GOSSIP_SENDER_MAIN, 1045);
                        player->SEND_GOSSIP_MENU(DEFAULT_GOSSIP_MESSAGE, item->GetGUID());
                    }
                    break;

                }
                break;
            case 1001:
                //player->ApplyEnchantment(item, PERM_ENCHANTMENT_SLOT, true);
                item->SetEnchantment(PERM_ENCHANTMENT_SLOT, ENCHAT_PRIEST_LVL1_1, 0, 0);
                ChatHandler(player->GetSession()).SendSysMessage("|cff00FA9AБонус успешно добавлен к вашему артефакту, сделайте релог|r");
                player->CLOSE_GOSSIP_MENU();
                break;
            case 1002:
                //player->ApplyEnchantment(item, PERM_ENCHANTMENT_SLOT, true);
                item->SetEnchantment(PERM_ENCHANTMENT_SLOT, ENCHAT_PRIEST_LVL1_2, 0, 0);
                ChatHandler(player->GetSession()).SendSysMessage("|cff00FA9AБонус успешно добавлен к вашему артефакту, сделайте релог|r");
                player->CLOSE_GOSSIP_MENU();
                break;
            case 1003:
                //player->ApplyEnchantment(item, PERM_ENCHANTMENT_SLOT, true);
                item->SetEnchantment(PERM_ENCHANTMENT_SLOT, ENCHAT_PRIEST_LVL1_3, 0, 0);
                ChatHandler(player->GetSession()).SendSysMessage("|cff00FA9AБонус успешно добавлен к вашему артефакту, сделайте релог|r");
                player->CLOSE_GOSSIP_MENU();
                break;
        }
    }
private:

    bool IsHaveItem(Player* player)
    {
        return player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FINGER1) ? true : false;
    }

    bool IsHaveEnchantment(Player* player)
    {
        if (!IsHaveItem(player))
            return false;

        Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FINGER1);

        if (!item->GetEnchantmentId(PERM_ENCHANTMENT_SLOT))
            return false;
        
        return true;
    }

    void AddGossipEnchantment(Player* player)
    {
        Item* item = player->GetItemByPos(INVENTORY_SLOT_BAG_0, EQUIPMENT_SLOT_FINGER1);
        if (!IsHaveEnchantment(player))
        {
            AddGossipItemFor(player, 0, "Я хочу Получить Бонус артефакта", GOSSIP_SENDER_MAIN, 102);
            AddGossipItemFor(player, 0, "Закрыть меню управления артефактом", GOSSIP_SENDER_MAIN, 101);
            player->SEND_GOSSIP_MENU(WORK_GOSSIP_MESSAGE, item->GetGUID());
            return;
        }            
        
        for (uint32 slot = PERM_ENCHANTMENT_SLOT; slot < MAX_ENCHANTMENT_SLOT; slot++)
        {

            uint32 enchantId = item->GetEnchantmentId(EnchantmentSlot(slot));
            if (!enchantId)
                continue;

            SpellItemEnchantmentEntry const* enchantEntry = sSpellItemEnchantmentStore.LookupEntry(enchantId);
            if (!enchantEntry)
                continue;

            for (uint32 enchants = 0; enchants < sizeof(enchantmentIds) / sizeof(uint32); enchants++)
            {
                if (enchantEntry->ID != enchantmentIds[enchants])
                    continue;

                AddGossipItemFor(player, 0, "Я хочу обновить Бонус Артефакта", GOSSIP_SENDER_MAIN, 100);
                AddGossipItemFor(player, 0, "Закрыть Меню", GOSSIP_SENDER_MAIN, 101);
                player->SEND_GOSSIP_MENU(WORK_GOSSIP_MESSAGE, item->GetGUID());
                
            }
        }
    }
    
};

bool LearnAllRecipesInProfession(Player* pPlayer, SkillType skill)
{
    ChatHandler handler(pPlayer->GetSession());
    char* skill_name;

    SkillLineEntry const* SkillInfo = sSkillLineStore.LookupEntry(skill);
    skill_name = SkillInfo->name[handler.GetSessionDbcLocale()];


    uint32 targetHasSkill = pPlayer->GetSkillValue(skill);
    uint16 maxLevel = pPlayer->GetPureMaxSkillValue(skill);

    if (maxLevel > 450)
    {
        return true;
    }

    pPlayer->SetSkill(SkillInfo->id, pPlayer->GetSkillStep(SkillInfo->id), targetHasSkill + 25, maxLevel);
    handler.PSendSysMessage("Навык %c повышен на 25", skill_name);

    return true;
}

class skill_item_ALCHEMY : public ItemScript
{
public:
    skill_item_ALCHEMY() : ItemScript("skill_item_ALCHEMY") { }

    bool OnUse(Player* player, Item* item, SpellCastTargets const& /*targets*/) override // Any hook here
    {
        ChatHandler handler(player->GetSession());
        player->PlayerTalkClass->ClearMenus();
            LearnAllRecipesInProfession(player, SKILL_ALCHEMY);
            return true;
    }
};
class skill_item_BLACKSMITHING : public ItemScript
{
public:
    skill_item_BLACKSMITHING() : ItemScript("skill_item_BLACKSMITHING") { }

    bool OnUse(Player* player, Item* item, SpellCastTargets const& /*targets*/) override // Any hook here
    {
        ChatHandler handler(player->GetSession());
        player->PlayerTalkClass->ClearMenus();
            LearnAllRecipesInProfession(player, SKILL_BLACKSMITHING);
            return true;
    }
};
class skill_item_LEATHERWORKING : public ItemScript
{
public:
    skill_item_LEATHERWORKING() : ItemScript("skill_item_LEATHERWORKING") { }

    bool OnUse(Player* player, Item* item, SpellCastTargets const& /*targets*/) override // Any hook here
    {
        ChatHandler handler(player->GetSession());
        player->PlayerTalkClass->ClearMenus();
            LearnAllRecipesInProfession(player, SKILL_LEATHERWORKING);
            return true;
    }
};
class skill_item_TAILORING : public ItemScript
{
public:
    skill_item_TAILORING() : ItemScript("skill_item_TAILORING") { }

    bool OnUse(Player* player, Item* item, SpellCastTargets const& /*targets*/) override // Any hook here
    {
        ChatHandler handler(player->GetSession());
        player->PlayerTalkClass->ClearMenus();
            LearnAllRecipesInProfession(player, SKILL_TAILORING);
            return true;
    }
};
class skill_item_ENGINEERING : public ItemScript
{
public:
    skill_item_ENGINEERING() : ItemScript("skill_item_ENGINEERING") { }

    bool OnUse(Player* player, Item* item, SpellCastTargets const& /*targets*/) override // Any hook here
    {
        ChatHandler handler(player->GetSession());
        player->PlayerTalkClass->ClearMenus();
            LearnAllRecipesInProfession(player, SKILL_ENGINEERING);
            return true;
    }
};
class skill_item_ENCHANTING : public ItemScript
{
public:
    skill_item_ENCHANTING() : ItemScript("skill_item_ENCHANTING") { }

    bool OnUse(Player* player, Item* item, SpellCastTargets const& /*targets*/) override // Any hook here
    {
        ChatHandler handler(player->GetSession());
        player->PlayerTalkClass->ClearMenus();
            LearnAllRecipesInProfession(player, SKILL_ENCHANTING);
            return true;
    }
};
class skill_item_JEWELCRAFTING : public ItemScript
{
public:
    skill_item_JEWELCRAFTING() : ItemScript("skill_item_JEWELCRAFTING") { }

    bool OnUse(Player* player, Item* item, SpellCastTargets const& /*targets*/) override // Any hook here
    {
        ChatHandler handler(player->GetSession());
        player->PlayerTalkClass->ClearMenus();
            LearnAllRecipesInProfession(player, SKILL_JEWELCRAFTING);
            return true;
    }
};
class skill_item_INSCRIPTION : public ItemScript
{
public:
    skill_item_INSCRIPTION() : ItemScript("skill_item_INSCRIPTION") { }

    bool OnUse(Player* player, Item* item, SpellCastTargets const& /*targets*/) override // Any hook here
    {
        ChatHandler handler(player->GetSession());
        player->PlayerTalkClass->ClearMenus();
            LearnAllRecipesInProfession(player, SKILL_INSCRIPTION);
            return true;
    }
};


void AddSC_artt_command() // Add to scriptloader normally
{
    //new artt_command();
    new artt_command();

    //итемы на поднятия навыка профы
    new skill_item_ALCHEMY();
    new skill_item_BLACKSMITHING();
    new skill_item_LEATHERWORKING();
    new skill_item_TAILORING();
    new skill_item_ENGINEERING();
    new skill_item_ENCHANTING();
    new skill_item_JEWELCRAFTING();
    new skill_item_INSCRIPTION();
    //
}
